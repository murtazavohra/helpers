//
//  PropertyInittializable.swift
//  maps
//
//  Created by Yurei on 22/12/18.
//  Copyright © 2018 Yurei. All rights reserved.
//

import UIKit

protocol Identifiable { }

extension Identifiable {
    static var identifier: String {
        return String(describing: Self.self)
    }
}

protocol PropertyInitializable { }

extension PropertyInitializable where Self: UIView {
    static var usingAL: Self {
        let instance = Self.init(frame: .zero)
        instance.translatesAutoresizingMaskIntoConstraints = false
        return instance
    }
    
    static func usingAL(_ handler: ((Self) -> Void)) -> Self {
        let instance = Self.init(frame: .zero)
        instance.translatesAutoresizingMaskIntoConstraints = false
        handler(instance)
        return instance
    }
}

extension UIView: PropertyInitializable { }
