//
//  FormData.swift
//  maps
//
//  Created by Yurei on 02/02/19.
//  Copyright © 2019 Yurei. All rights reserved.
//

import Foundation

class FormData {
    enum Part {
        case text(name: String, String)
        case file(name: String, fileName: String, mimeType: String, Data)
        
        func create(boundary: String) -> Data {
            var partData = Data()
            switch self {
            case .text(let name, let text):
                partData.append("Content-Disposition: form-data; name=\"\(name)\"".data(using: .utf8)!)
                partData.append(text.data(using: .utf8)!)
                partData.append("\r\n".data(using: .utf8)!)
                return partData
                
            case .file(let name, let fileName, let mimeType, let data):
                partData.append("Content-Disposition: form-data; name=\"\(name)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
                partData.append("Content-Type: \(mimeType)\r\n\r\n".data(using: .utf8)!)
                partData.append(data)
                partData.append("\r\n".data(using: .utf8)!)
                return partData
            }
        }
    }
    
    private var data: Data
    private var parts: [Part] = []
    private let boundary: String
    
    init(_ boundary: String) {
        self.boundary = boundary
        data = Data()
        data.append("--\(boundary)\r\n".data(using: .utf8)!)
    }
    
    func add(part: Part) -> FormData {
        data.append(part.create(boundary: boundary))
        return self
    }
    
    func build() -> Data {
        data.append("--\(boundary)--\r\n".data(using: .utf8)!)
        return data
    }
}
