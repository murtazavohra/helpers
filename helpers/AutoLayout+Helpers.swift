//
//  AutoLayout+Helpers.swift
//  helpers
//
//  Created by Yurei on 22/12/18.
//  Copyright © 2018 Yurei. All rights reserved.
//

import UIKit

extension UIView {
    var top: NSLayoutYAxisAnchor {
        return topAnchor
    }
    
    var left: NSLayoutXAxisAnchor {
        return leadingAnchor
    }
    
    var bottom: NSLayoutYAxisAnchor {
        return bottomAnchor
    }
    
    var right: NSLayoutXAxisAnchor {
        return trailingAnchor
    }
    
    var centerX: NSLayoutXAxisAnchor {
        return centerXAnchor
    }
    
    var centerY: NSLayoutYAxisAnchor {
        return centerYAnchor
    }
    
    var height: NSLayoutDimension {
        return heightAnchor
    }
    
    var width: NSLayoutDimension {
        return widthAnchor
    }
    
    @discardableResult
    func constrain(to view: UIView) -> [NSLayoutConstraint] {
        return [top.constrain(to: view.top),
                left.constrain(to: view.left),
                bottom.constrain(to: view.bottom),
                right.constrain(to: view.right)]
    }
}

extension NSLayoutYAxisAnchor {
    @discardableResult
    func constrain(to anchor: NSLayoutYAxisAnchor) -> NSLayoutConstraint {
        return constraint(equalTo: anchor).activate()
    }
}

extension NSLayoutXAxisAnchor {
    @discardableResult
    func constrain(to anchor: NSLayoutXAxisAnchor) -> NSLayoutConstraint {
        return constraint(equalTo: anchor).activate()
    }
}

extension NSLayoutDimension {
    @discardableResult
    func constrain(to anchor: NSLayoutDimension) -> NSLayoutConstraint {
        return constraint(equalTo: anchor).activate()
    }
    
    @discardableResult
    func constrain(to constant: CGFloat) -> NSLayoutConstraint {
        return constraint(equalToConstant: constant).activate()
    }
}

extension NSLayoutConstraint {
    @discardableResult
    func activate() -> NSLayoutConstraint {
        isActive = true
        return self
    }
    
    @discardableResult
    func deactivate() -> NSLayoutConstraint {
        isActive = false
        return self
    }
    
    @discardableResult
    func with(constant: CGFloat) -> NSLayoutConstraint {
        self.constant = constant
        return self
    }
}
