//
//  UIStackView+Helpers.swift
//  helpers
//
//  Created by Yurei on 22/12/18.
//  Copyright © 2018 Yurei. All rights reserved.
//

import UIKit

extension UIStackView {
    convenience init(_ axis: NSLayoutConstraint.Axis, _ distribution: Distribution, _ alignment: Alignment, _ spacing: CGFloat) {
        self.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        
        self.axis = axis
        self.distribution = distribution
        self.alignment = alignment
        self.spacing = spacing
    }
    
    func inset(by insets: Insets) {
        isLayoutMarginsRelativeArrangement = true
        directionalLayoutMargins = insets.directionalInsets
    }
}
