//
//  Insets.swift
//  helpers
//
//  Created by Yurei on 22/12/18.
//  Copyright © 2018 Yurei. All rights reserved.
//

import UIKit

struct Insets {
    var top: CGFloat
    var left: CGFloat
    var bottom: CGFloat
    var right: CGFloat
    
    var edgeInsets: UIEdgeInsets {
        return UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }
    
    var directionalInsets: NSDirectionalEdgeInsets {
        return NSDirectionalEdgeInsets(top: top, leading: left, bottom: bottom, trailing: right)
    }
    
    init(_ top: CGFloat, _ left: CGFloat, _ bottom: CGFloat, _ right: CGFloat) {
        self.top = top
        self.left = left
        self.bottom = bottom
        self.right = right
    }
    
    init(_ common: CGFloat) {
        self.top = common
        self.left = common
        self.bottom = common
        self.right = common
    }
}
